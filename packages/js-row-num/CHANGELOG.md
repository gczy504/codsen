# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 2.4.0 (2019-10-05)

### Features

- opts.triggerKeywords ([d76cf72](https://gitlab.com/codsen/codsen/commit/d76cf72))
- opts.triggerKeywords ([8ef01ae](https://gitlab.com/codsen/codsen/commit/8ef01ae))

## 2.3.0 (2019-06-18)

### Features

- Remove check-types-mini dependency to make it more efficient ([c36ce2f](https://gitlab.com/codsen/codsen/commit/c36ce2f))

## 2.2.0 (2019-06-01)

### Features

- Lower the 4 digit padding threshold to 45K characters ([e5f8ec7](https://gitlab.com/codsen/codsen/commit/e5f8ec7))

## 2.1.0 (2019-04-06)

### Features

- Bump padding to four if input is longer than 50K characters ([6855b53](https://gitlab.com/codsen/codsen/commit/6855b53))

## 1.3.0 (2019-01-20)

- ✨ Various documentation and setup tweaks after we migrated to monorepo
- ✨ Setup refresh: updated dependencies and all config files using automated tools

## 1.1.0 (2018-12-26)

- ✨ `String.padStart` is Ecmascript 2017 which limits the Node versions we can support, so we replaced it ([5a49a2e](https://gitlab.com/codsen/codsen/tree/master/packages/js-row-num/commits/5a49a2e))

## 1.0.0 (2017-07-11)

- ✨ First public release
