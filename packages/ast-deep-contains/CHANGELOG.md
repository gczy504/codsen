# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2019-11-11)

### Features

- init ([d388611](https://gitlab.com/codsen/codsen/commit/d38861123f7c305e8e34a338fbbfa2c6b1e5a930))
- pass the path as the third argument to cb() ([85574cd](https://gitlab.com/codsen/codsen/commit/85574cd26daf82bb65325529c1d3faa9fd348005))

### BREAKING CHANGES

- initial release

## 1.0.0 (2019-11-10)

- ✨ First public release.
