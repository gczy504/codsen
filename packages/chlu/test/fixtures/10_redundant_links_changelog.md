# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.2.0] - 2017-04-21
### Added
- blablabla
- blablabla
### Improved
- blablabla
### Updated
- Readme
### Unchanged
- Code coverage is still 100%

## [1.1.0] - 2017-04-20
### Added
- blablabla
- blablabla
### Improved
- blablabla
### Updated
- Readme
### Unchanged
- Code coverage is still 100%

## 1.0.0 - 2017-04-03
### New
- First public release

[2.0.0]: https://github.com/wrong-user/wrong-lib/compare/v2.0.0...v1.9.0
[1.2.0]: https://github.com/whatever/wrong-lib/compare/v1.1.0...v1.2.0
[1.9.0]: https://github.com/zzz/wrong-lib/compare/v1.8.0...v1.9.0
