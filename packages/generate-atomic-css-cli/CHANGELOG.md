# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.1.0 (2019-07-19)

### Features

- Initial release ([d4b824b](https://gitlab.com/codsen/codsen/commit/d4b824b))

## 1.0.0 (2019-07-17)

- ✨ First public release
