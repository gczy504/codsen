# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2019-06-29)

### Features

- Uglifies to single letter names by shortening to first letter ([02cb90b](https://gitlab.com/codsen/codsen/commit/02cb90b))

## 1.1.0 (2019-06-22)

### Features

- Fully recode the algorithm to make it 99.9% resilient to positional reference array changes ([5c364a8](https://gitlab.com/codsen/codsen/commit/5c364a8))

## 1.0.0 (2019-06-21)

- ✨ First public release
