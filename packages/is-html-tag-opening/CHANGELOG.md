# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.2.0 (2019-11-18)

### Features

- improve the algorithm to include way more broken tag cases ([fd94b61](https://gitlab.com/codsen/codsen/commit/fd94b61d39c1a4e4e0275e4e57cbde4b884db4c1))
- loosen the requirements for the character that follows recognised tag name sequence ([6201e2b](https://gitlab.com/codsen/codsen/commit/6201e2b8a2048a64239bcf4893404eeaba3b3d2b))

## 1.1.0 (2019-11-02)

### Features

- init ([04dfb3b](https://gitlab.com/codsen/codsen/commit/04dfb3b1937ad472a6ed615e8ca479a37f8cb9bb))
- recognise any html tags which start with bracket followed by a known tag name ([3ac6327](https://gitlab.com/codsen/codsen/commit/3ac6327d2258a36322dc6d5411cb3b1dad392d3e))

## 1.0.0 (2019-11-01)

- ✨ First public release
